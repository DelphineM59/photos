<?php namespace App\Controllers;
use CodeIgniter\Controller;

class Caccueil extends Controller
{
	public function index()
	{
		$data['page_title'] = "Concours photographique";
		$data['titre1'] = "Concours photographique";

		$page['contenu'] = view('v_acceuil', $data);
		return view('Commun/v_template', $page);
	}

	//--------------------------------------------------------------------

}
