<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Mcompetition;
use App\Models\Mphoto;
use \CodeIgniter\Exceptions\PageNotFoundException;

class Ccompetition extends Controller
{
	
	public function index()
	{
		$model = new Mcompetition();
		$data['result'] = $model->getAll();

		$data['page_title'] = "Concours photographique";
		$data['titre1'] = "Concours photographique";

		$page['contenu'] = view('competition/V_liste_competition', $data);
		return view('Commun/v_template', $page);
	}

	public function detail($prmId = null)
	{
		if ($prmId != null) {
			$model = new Mcompetition();
			$data['result'] = $model->getDetail($prmId);
			if (count($data['result']) != 0) {
				$modlPhoto = new Mphoto();
				$data['resultPhoto'] = $modlPhoto->getAllById($prmId);
				$data['page_title'] = "Le classement";

				$data['titre1'] = "La competition ".$data['result'][0]['Nom'];
				$data['dateCompetition'] = $data['result'][0]['Date'];
				$page['contenu'] = view('competition/v_detail_competition', $data);
				return view('Commun/v_template', $page);
			} else {
				throw PageNotFoundException::forPageNotFound("Cette compétition n'existe pas !");
			}
		} else {
			throw PageNotFoundException::forPageNotFound("Il faut choisir une compétition !");
		}
	}

	public function detailPhoto($prmId = null){
		
		if ($prmId != null) {
			$model = new Mphoto();
			$data['result'] = $model->getDetail($prmId);

		if (count($data['result']) != 0) {

			$data['page_title'] = "Le classement";
			$data['titre1'] = $data['result'][0]['Titre'];


			$page['contenu'] = view('competition/v_detail_photo', $data);
			return view('Commun/v_template', $page);
		} else {
			throw PageNotFoundException::forPageNotFound("Cette photo n'existe pas !");
		}
		} else {
		throw PageNotFoundException::forPageNotFound("Il faut choisir une photo !");
		}
	}
}