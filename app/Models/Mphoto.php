<?php

namespace App\Models;

use CodeIgniter\Model;

class Mphoto extends Model
{
    protected $table = 'photo';
    protected $primaryKey = 'ID';
    protected $returnType = 'array';

    public function getAllById($prmIdCompetition)
    {
        $requete = $this->select('photo.ID, Titre,Classement,concurrent.Nom,Prenom,Pays')
        ->join('concurrent','photo.concurrentID=concurrent.ID', 'left')
        ->where(['photo.competitionID'=>$prmIdCompetition])
        ->orderby('Classement','asc');
        return $requete->findAll();
    }  
    
    public function getDetail($prmId)
    {
        return $this->select('Titre,Classement,concurrent.Nom,Prenom,Pays,NomFichier,Total,DossierStockage,competition.Nom as CNom')
        ->join('concurrent','photo.concurrentID=concurrent.ID', 'left')
        ->join('competition','photo.competitionID=competition.ID','left')
        ->where(['photo.ID'=>$prmId])
        ->findAll();
        
        
        //return $requete->findAll();
    }  
}
