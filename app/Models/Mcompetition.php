<?php

namespace App\Models;

use CodeIgniter\Model;

class Mcompetition extends Model
{
    protected $table = 'competition';
    protected $primaryKey = 'ID';
    protected $returnType = 'array';

    public function getAll()
    {
        $requete = $this->select('ID, Nom,DossierStockage,Date');
        return $requete->findAll();
    }


    public function getDetail($prmId)
    {
        // $requete = $this->select('*')->where(['Id' => $prmId]);
        return $this->select('*')
        ->join('photo','competition.ID=photo.ID', 'left')
        ->where(['competition.ID'=>$prmId])
        ->orderby('Classement','asc')
        ->findAll();
    }
    
}
