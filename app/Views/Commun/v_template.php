<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page_title;?></title>
    <link href="<?= base_url("css/styles.css")?>" rel="stylesheet"  type="text/css" />
</head>

<body>
    <div id="conteneur">
        <header>
            <h1><?php echo $page_title;?></h1>
        </header>

        <nav>
            <ul>
                <li><a href="<?= base_url("Caccueil") ?>">Accueil</a></li>
                <li><a href="<?= base_url("Ccompetition") ?>">Les compétitions</a></li>
            </ul>
        </nav>
        <section>
        <?php echo $contenu;?>
        </section>

<footer>
<p>Copyright - Tous droits réservés - <a href="#">Contact</a></p>
</footer>
</div>
</body>

</html>